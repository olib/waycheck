# Waycheck

Waycheck is a simple Qt6 application that displays all protocols implemented by the compositor that it's running in.

## License

Waycheck is licensed under the `Apache-2.0` open-source license.

The logomark is inspired by the Wayland logomark, and was co-designed by Campbell Jones (@serebit) and Oliver Beard (@olib). It is dual-licensed under the terms of the `CC0-1.0` and `Apache-2.0` open-source licenses.
