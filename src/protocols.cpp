#include "protocols.hpp"

std::string status_to_string(ProtocolStatus status) {
    switch (status) {
        case STABLE:
            return "Stable";
        case STAGING:
            return "Staging";
        case UNSTABLE:
            return "Unstable";
        case WLROOTS:
            return "Wlroots";
        case KDE:
            return "KDE";
        case WESTON:
            return "Weston";
        default:
            return "Unknown";
    }
}
