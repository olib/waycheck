#include "window.hpp"

#include <QApplication>
#include <QMessageBox>

int main(int argc, char** argv) {
    auto app = QApplication(argc, argv);

    app.setDesktopFileName(QStringLiteral("dev.serebit.Waycheck.desktop"));

    auto waylandApp = qApp->nativeInterface<QNativeInterface::QWaylandApplication>();
    QMessageBox msgBox;
    if (waylandApp == nullptr) {
        msgBox.setText("This application is running outside of a Wayland session, and cannot start.");
        return msgBox.exec();
    }

    auto window = Window(waylandApp);
    window.show();

    return app.exec();
}
