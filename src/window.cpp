#include "window.hpp"

#include "protocols.hpp"
#include "ui_window.h"

#include <QAbstractItemView>
#include <QCheckBox>
#include <QGuiApplication>
#include <QAbstractItemView>
#include <fstream>
#include <sys/socket.h>
#include <unistd.h>
#include <wayland-client-protocol.h>
#include <wayland-client.h>

static pid_t pid_from_fd(int fd) {
    struct ucred ucred;
    socklen_t len = sizeof(struct ucred);
    if (getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &ucred, &len) == -1) {
        perror("getsockopt failed");
        exit(1);
    }
    return ucred.pid;
}

static std::string process_name_from_pid(const pid_t pid) {
    std::string procpath = QString::asprintf("/proc/%d/comm", pid).toStdString();

    std::ifstream infile(procpath);
    if (infile.is_open()) {
        std::string out;
        std::getline(infile, out);
        infile.close();
        return out;
    } else if (getenv("container") != nullptr) {
        // running in a flatpak, most likely
        return "Unknown (Sandboxed)";
    } else {
        return "Unknown";
    }
}

static void registry_global(void* data, wl_registry* registry, uint32_t name, const char* interface, uint32_t version) {
    (void) registry;
    (void) name;

    if (std::string(interface).starts_with("wl_")) {
        return;
    }

    auto* window = static_cast<Window*>(data);
    window->newGlobal(interface, version);
}

static void registry_global_remove(void* data, struct wl_registry* registry, uint32_t name) {
    (void) data;
    (void) registry;
    (void) name;
}

Window::Window(QNativeInterface::QWaylandApplication* waylandApp, QWidget* parent)
    : QMainWindow(parent), upstreamModel(0, 4), wlrootsModel(0, 3), kdeModel(0, 3), westonModel(0, 3), ui(std::make_unique<Ui::Window>()) {
    ui->setupUi(this);

    initTable(upstreamModel, *ui->upstreamTable);
    initTable(wlrootsModel, *ui->wlrootsTable, false);
    initTable(kdeModel, *ui->kdeTable, false);
    initTable(westonModel, *ui->westonTable, false);

    for (auto protocol : known_protocols) {
        addProtocol(protocol.second);
    }

    ui->upstreamTable->sortByColumn(1, Qt::AscendingOrder);
    for (auto* table : {ui->upstreamTable, ui->wlrootsTable, ui->kdeTable, ui->westonTable}) {
        table->sortByColumn(0, Qt::AscendingOrder);
    }

    auto* display = waylandApp->display();
    auto fd = wl_display_get_fd(display);
    auto pid = pid_from_fd(fd);
    auto pname = process_name_from_pid(pid);
    ui->compositor->setText(QString::asprintf("Compositor: %s", pname.c_str()));

    auto listener = wl_registry_listener{.global = registry_global, .global_remove = registry_global_remove};

    auto* registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &listener, (void*) this);
    wl_display_roundtrip(display);
}

Window::~Window() = default;

void Window::newGlobal(const std::string interface, const uint32_t version) {
    const Protocol& protocol = known_protocols[interface];
    if (protocol.status == UNKNOWN) {
        return;
    }

    auto* model = modelForStatus(protocol.status);
    if (model == nullptr) {
        return;
    }

    auto column = (model == &upstreamModel) ? 2 : 1;
    auto matches = model->findItems(QString::fromStdString(protocol.name), Qt::MatchExactly, column);

    for (auto match : matches) {
        auto versionItem = model->item(match->row(), column + 1);
        versionItem->setCheckState(Qt::Checked);
        versionItem->setText(QString::asprintf("%d", version));

        for (auto i = 0; i < model->columnCount(); i++) {
            auto item = model->item(match->row(), i);
            item->setData(true, Qt::UserRole);
        }
    }
}

void Window::addProtocol(const Protocol& protocol) {
    auto* model = modelForStatus(protocol.status);
    if (model == nullptr) {
        return;
    }

    auto versionItem = new QStandardItem(false);
    versionItem->setCheckState(Qt::Unchecked);
    versionItem->setText("N/A");

    auto items = QList<QStandardItem*>({
        new QStandardItem(QString::fromStdString(protocol.pretty)),
        new QStandardItem(QString::fromStdString(protocol.name)),
        versionItem,
    });
    if (protocol.status <= UNSTABLE) {
        auto* statusItem = new QStandardItem(QString::fromStdString(status_to_string(protocol.status)));
        statusItem->setTextAlignment(Qt::AlignCenter);
        items.prepend(statusItem);
    }

    for (auto item : std::as_const(items)) {
        item->setData(false, Qt::UserRole);
    }

    model->appendRow(items);
}

void Window::initTable(QStandardItemModel& model, QTableView& table, bool includeStatus) {
    table.setModel(&model);
    table.setSortingEnabled(true);

    if (includeStatus) {
        model.setHorizontalHeaderLabels({"Status", "Name", "Identifier", "Implementation"});
        table.horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        table.horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
        table.horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
        table.horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    } else {
        model.setHorizontalHeaderLabels({"Name", "Identifier", "Implementation"});
        table.horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
        table.horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
        table.horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    }
    table.sortByColumn(0, Qt::AscendingOrder);

    table.setEditTriggers(QAbstractItemView::NoEditTriggers);
    table.setSelectionMode(QAbstractItemView::SingleSelection);
    table.verticalHeader()->setVisible(false);
    table.setItemDelegate(new TableItemDelegate);
}

QStandardItemModel* Window::modelForStatus(const ProtocolStatus status) {
    switch (status) {
        case STABLE:
        case STAGING:
        case UNSTABLE:
            return &upstreamModel;
        case WLROOTS:
            return &wlrootsModel;
        case KDE:
            return &kdeModel;
        case WESTON:
            return &westonModel;
        default:
            return nullptr;
    }
}

TableItemDelegate::TableItemDelegate(QObject* parent) : QStyledItemDelegate(parent) {}

void TableItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const {
    bool enabled = index.data(Qt::UserRole).toBool();

    if (!enabled) {
        QStyleOptionViewItem newOption(option);
        initStyleOption(&newOption, index);

        QColor textColor = newOption.palette.text().color();
        textColor.setAlphaF(0.6);
        newOption.palette.setColor(QPalette::Text, textColor);

        QStyledItemDelegate::paint(painter, newOption, index);
    } else {
        QStyledItemDelegate::paint(painter, option, index);
    }
}
