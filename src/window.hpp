#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "protocols.hpp"

#include <QGuiApplication>
#include <QMainWindow>
#include <QStandardItemModel>
#include <QStyledItemDelegate>
#include <QTableView>

#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
class Window;
}
QT_END_NAMESPACE

class Window : public QMainWindow {
    Q_OBJECT
    QStandardItemModel upstreamModel;
    QStandardItemModel wlrootsModel;
    QStandardItemModel kdeModel;
    QStandardItemModel westonModel;

  public:
    Window(QNativeInterface::QWaylandApplication* waylandApp, QWidget* parent = nullptr);
    ~Window();

    void newGlobal(const std::string interface, const uint32_t version);

    void addProtocol(const Protocol& protocol);

  private:
    void initTable(QStandardItemModel& model, QTableView& table, bool includeStatus = true);

    QStandardItemModel* modelForStatus(const ProtocolStatus status);

  private:
    std::unique_ptr<Ui::Window> const ui;
};

class TableItemDelegate : public QStyledItemDelegate {
  public:
    TableItemDelegate(QObject* parent = nullptr);

    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

#endif
